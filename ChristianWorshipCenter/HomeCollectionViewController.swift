//
//  HomeCollectionViewController.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/29/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import LIHImageSlider

private let reuseIdentifier = "cell"

class HomeCollectionViewController: UICollectionViewController, LIHSliderDelegate {
    
    // Custom image slider
    fileprivate var sliderVc1: LIHSliderViewController!
    
    // Class var store image text and image name
    var coolImage: [UIImage] = []
    //var imageText: [String] = []
    
    let images: [UIImage] = [UIImage(named: "ontimeword.jpg")!,UIImage(named: "ontimepraise.jpg")!,UIImage(named: "give.jpg")!]

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Remove back button title
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        // Cell image loop through 6 images
        /*for i in 2..<8 {
            
            let image = UIImage(named: "\(i).jpg")
            self.coolImage.append(image!)
        }*/
        self.coolImage = [UIImage(named: "ui.jpg")!,UIImage(named: "dance.jpg")!,UIImage(named: "marriage.jpg")!,UIImage(named: "youth.jpg")!,UIImage(named: "wow.jpg")!,UIImage(named: "mod.jpg")!]
        // Cell image description
        //imageText = ["Urban","Dance","Marriage","Kids","Women","Men"]
        
        //Image slider configurations
        // Slider image name
        ///let images: [UIImage] = [UIImage(named: "ontimeword.jpg")!,UIImage(named: "ontimepraise.jpg")!,UIImage(named: "give.jpg")!]
        
        // Add slider image to slider
        ///let slider1: LIHSlider = LIHSlider(images: images)
        
        // Slider image description
        //slider1.sliderDescriptions = ["Image 1 description","Image 2 description","Image 3 description"]
        
        // Add slider to slider VC
        ///self.sliderVc1  = LIHSliderViewController(slider: slider1)
        ///sliderVc1.delegate = self
        
        // Add as VC child
        ///self.addChildViewController(self.sliderVc1)
        
        // Add as subview
        ///self.sliderVc1.didMove(toParentViewController: self)
        ///slider1.showPageIndicator = false
    }
    
    /*override func viewDidLayoutSubviews() {
        
        self.sliderVc1!.view.frame = self.slider1Container.frame
    }*/
    
    // Trigger view when image slider image is pressed
    func itemPressedAtIndex(index: Int) {
        
        print("index \(index) is pressed")
        
        switch index {
        case 0:
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/21"
                customWebViewController.pageTitle = "Pastors"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case 1:
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/56"
                customWebViewController.pageTitle = "Praise"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case 2:
            if let storyboard = storyboard {
                
                let popWebViewController = storyboard.instantiateViewController(withIdentifier: "PopWebViewController") as! PopWebViewController
                popWebViewController.url = "https://itunes.apple.com/us/app/givelify/id725815127"
                //popWebViewController.pageTitle = "Givelify"
                
                let popWebViewNav = UINavigationController(rootViewController: popWebViewController)
                
                popWebViewNav.isNavigationBarHidden = false
                popWebViewNav.navigationBar.isTranslucent = false
                popWebViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                popWebViewNav.navigationBar.shadowImage = UIImage()
                popWebViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                popWebViewNav.navigationBar.tintColor = UIColor.white
                popWebViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(popWebViewNav, animated: true, completion: nil)
            }
            break
        default:
            break
        }
    }

    // Trigger toggle slide out menu
    @IBAction func leftSideBarItemTapped(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return coolImage.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        let item = coolImage[indexPath.row]
        //let text = imageText[indexPath.row]
        
        cell.myImageView.image = item
        //cell.textLabel.text = text
        cell.textLabel.isHidden = true
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch (indexPath.row) {
            
        //
        case (0):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/122"
                //customWebViewController.url = "https://www.handytaxguy.com/wp-json/wp/v2/pages/21"
                customWebViewController.pageTitle = "Urban"
                
                //let webViewNav = UINavigationController(rootViewController: webViewController)
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
                
                /*webViewNav.isNavigationBarHidden = false
                 webViewNav.navigationBar.isTranslucent = false
                 webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                 webViewNav.navigationBar.shadowImage = UIImage()
                 webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                 webViewNav.navigationBar.tintColor = UIColor.white
                 webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]*/
                
                //let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                //appDelegate.drawerContainer!.centerViewController = webViewNav
                //appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (1):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/314"
                customWebViewController.pageTitle = "Dance"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case (2):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/124"
                customWebViewController.pageTitle = "Marriage"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case (3):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/134"
                customWebViewController.pageTitle = "Youth"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case (4):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/132"
                customWebViewController.pageTitle = "WOW"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        case (5):
            if let storyboard = storyboard {
                
                let customWebViewController = storyboard.instantiateViewController(withIdentifier: "CustomWebViewController") as! CustomWebViewController
                customWebViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/130"
                customWebViewController.pageTitle = "MOD"
                
                self.navigationController?.pushViewController(customWebViewController, animated: false)
            }
            break
        default:
            break
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! HeaderCollectionReusableView
        
        let sliderRatio: CGFloat = 1 / 2 // slider height ratio
        
        // Add slider image to slider
        let slider1: LIHSlider = LIHSlider(images: images)
        
        // Add slider to slider VC
        self.sliderVc1  = LIHSliderViewController(slider: slider1)
        sliderVc1.delegate = self
        
        // Add as VC child
        self.addChildViewController(self.sliderVc1)
        
        // Add as subview
        self.sliderVc1.didMove(toParentViewController: self)
        slider1.showPageIndicator = false
        
        header.addSubview(self.sliderVc1.view)
        
        // Slider view alligment
        sliderVc1.view.frame = CGRect(x: header.frame.origin.x, y: header.frame.origin.y, width: header.frame.size.width, height: (header.frame.size.width * sliderRatio) - header.textLabel.frame.size.height)
        
        return header
    }
}

// MARK: UICollectionViewDelegateFlowLayout

extension HomeCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    // Config cell size for different size phones
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemSize = self.view.bounds.width / 3
        
        return CGSize(width: itemSize, height: itemSize)
    }
    
    // Config cell spacing for different size phones
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let width = self.view.bounds.width
        let height = self.view.bounds.height
        // Top Left Bottom Right
        
        return UIEdgeInsetsMake(height / 66.7, width / 9.375, height / 66.7, width / 9.375)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let sliderRatio: CGFloat = 1 / 2 // slider height ratio
        
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * sliderRatio)
    }
}
