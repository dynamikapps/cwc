//
//  StandardWebViewController.swift
//  ChristianWorshipCenter
//
//  Created by Handy Metellus on 8/4/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit

class StandardWebViewController: UIViewController {
    
    // Class variables
    var url: String?
    //var pageTitle: String?
    var webViewHeight: CGFloat = 0.0
    var theBool: Bool!
    var myTimer: Timer!

    // Class UI Objects
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var toolbarBackBtn: UIBarButtonItem!
    @IBOutlet weak var toolbarForwardBtn: UIBarButtonItem!
    @IBOutlet weak var toolbarShareBtn: UIBarButtonItem!
    @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var progressbar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webView.delegate = self
        webView.scrollView.delegate = self
        
        // Set webview height to contraints value
        webViewHeight = webViewHeightConstraint.constant
        
        // Web history notification
        //NotificationCenter.default.addObserver(self, selector: #selector(StandardWebViewController.webViewHistoryDidChange), name: NSNotification.Name(rawValue: "WebHistoryItemChangedNotification"), object: nil)
        
        // New back button
        self.navigationItem.hidesBackButton = true
        let backButton = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        self.navigationItem.leftBarButtonItem = backButton
        
        // Swipe to go back
        let backSwipe = UISwipeGestureRecognizer(target: self, action: #selector(back))
        backSwipe.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(backSwipe)

        // Load pass page url or default page
        if let url = url {

            print("The request: \(url)")
            let myUrl = URL(string: url)
            let request = URLRequest(url: myUrl!)
            
            webView.loadRequest(request)
            
//            if let title = pageTitle {
//                var str = title.stringByRemovingAll(subStrings: myPIds)
//                str = str.components(separatedBy: ".com")[0]
//                str = str.components(separatedBy: ".net")[0]
//                str = str.components(separatedBy: ".com")[0]
//                str = str.components(separatedBy: ".edu")[0]
//                self.navigationItem.title = str
//            } else {
//                self.navigationItem.title = "CWC"
//            }
        } else {
            let requestURL = URL(string: "http://christianworshipcenters.com")
            print("The request: \(String(describing: requestURL))")
            
            let request = URLRequest(url: requestURL!)
            
            webView.loadRequest(request)
            
            self.navigationItem.title = "CWC"
        }
    }

    // MARK: Progress animation functions
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.progressbar.progress = 0.0
        self.theBool = false
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(PopWebViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.progressbar.progress >= 1 {
                self.progressbar.isHidden = true
                self.myTimer.invalidate()
            } else {
                self.progressbar.progress += 0.1
            }
        } else {
            self.progressbar.progress += 0.05
            if self.progressbar.progress >= 0.95 {
                self.progressbar.progress = 0.95
            }
        }
    }

    // Back button action
    func back() {
        // push back
        if webView.canGoBack{
            print("Going back to internal page")
            webView.goBack()
        } else {
            print("Going back to main navigation view")
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func toolbarBackBtnTapped(_ sender: Any) {
        print("test back")
        if webView.canGoBack{
            print("Going back to internal page")
            webView.goBack()
        } else {
            print("Going back to main navigation view")
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func toolbarForwardBtnTapped(_ sender: Any) {
        print("test forward")
        if webView.canGoForward{
            print("Going forward to internal page")
            webView.goForward()
        }
    }
    
    // Share current view/page
    @IBAction func toolbarShareBtnTapped(_ sender: Any) {
        
        let height = UIScreen.main.bounds.height
        let webViewURL = webView.request?.url?.absoluteString
         
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            // It's an iPhone
            // Share tapped
            let firstActionItem = webViewURL ?? "http://christianworshipcenters.com"
            
            let actionViewController = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
            self.present(actionViewController, animated: true, completion: nil)
        case .pad:
            // It's an iPad
            let firstActionItem = webViewURL ?? "http://christianworshipcenters.com"
            
            let activityVC = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
            let nav = UINavigationController(rootViewController: activityVC)
            nav.modalPresentationStyle = UIModalPresentationStyle.popover
            let popover = nav.popoverPresentationController as UIPopoverPresentationController!
            activityVC.preferredContentSize = CGSize(width: height / 4.55, height: height / 4.55)
            popover?.sourceView = toolbarShareBtn.value(forKey: "view") as? UIView
            //popover?.sourceRect = CGRect(x: height / 7.0, y: height / 37.0, width: 0.0, height: 0.0)
            
            self.present(nav, animated: true, completion: nil)
            
        case .unspecified:
            // Share tapped
            let firstActionItem = webViewURL ?? "http://christianworshipcenters.com"
            
            let actionViewController = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
            self.present(actionViewController, animated: true, completion: nil)
        default:
            break
        }
    }
    
    // Update back and foward buttons
    func updateButton(webView: UIWebView) {

        if webView.canGoForward {
            toolbarForwardBtn.isEnabled = true
        } else {
            toolbarForwardBtn.isEnabled = false
        }
    }
    
    func webViewHistoryDidChange() {
        print("WebView History Did Change")
        self.updateButton(webView: self.webView)
    }
}

// MARK: UIWebViewDelegate

extension StandardWebViewController: UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        NetworkActivityIndicatorManager.networkOperationStarted()
        funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        NetworkActivityIndicatorManager.networkOperationFinished()
        funcToCallCalledWhenUIWebViewFinishesLoading()
        updateButton(webView: webView)
        self.navigationItem.title = webView.stringByEvaluatingJavaScript(from: "document.title")
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        NetworkActivityIndicatorManager.networkOperationFinished()
        funcToCallCalledWhenUIWebViewFinishesLoading()
        updateButton(webView: webView)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        updateButton(webView: webView)
//        if navigationType == .linkClicked {
//            if let storyboard = self.storyboard {
//                let VC = storyboard.instantiateViewController(withIdentifier: "StandardWebViewController") as! StandardWebViewController
//                VC.url = request.url?.absoluteString
//                VC.pageTitle = request.url?.absoluteString
//                self.navigationController?.pushViewController(VC, animated: true)
//            }
//            return false
//        }
        
        return true  //Changed line.
    }
}

// MARK: UIScrollViewDelegate

extension StandardWebViewController: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                self.toolbar.isHidden = true
                self.webViewHeightConstraint.constant = 0.0
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                self.toolbar.isHidden = false
                self.webViewHeightConstraint.constant = self.webViewHeight
                print("Unhide")
            }, completion: nil)
        }
    }
}
