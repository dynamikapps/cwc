//
//  ViewController.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/24/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import Alamofire

class WebViewController: UIViewController {
    
    // Class Variables
    var url: String?
    var pageTitle: String?
    var theBool: Bool!
    var myTimer: Timer!
    
    // Class UI Objects
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var progressbar: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        webView.delegate = self
        funcToCallWhenStartLoadingYourWebview()
        NetworkActivityIndicatorManager.networkOperationStarted()
        
        webView.scrollView.delegate = self
        //webView.scrollView.showsHorizontalScrollIndicator = false

        if let url = url {
            
            getPage(url: url)
            
            if let title = pageTitle {
                self.navigationItem.title = title
            } else {
                self.navigationItem.title = "CWC"
            }
        }
    }
    
    // MARK: Progress animation functions
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.progressbar.progress = 0.0
        self.theBool = false
        self.myTimer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(PopWebViewController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.theBool = true
    }
    
    func timerCallback() {
        if self.theBool {
            if self.progressbar.progress >= 1 {
                self.progressbar.isHidden = true
                self.myTimer.invalidate()
            } else {
                self.progressbar.progress += 0.1
            }
        } else {
            self.progressbar.progress += 0.05
            if self.progressbar.progress >= 0.95 {
                self.progressbar.progress = 0.95
            }
        }
    }
    
    // Trigger left side menu drawer
    @IBAction func leftSideBarItemTapped(_ sender: Any) {
        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    // Get post information from site using WP API
    func getPage(url: String) {
        
        //Sending http post request
        Alamofire.request(url, method: .get, parameters: nil).responseJSON
            {
                response in
                //printing response
                //print(response)
                //print(response.result)
                
                switch (response.result) {
                case .success( _):
                    if let result = response.result.value
                    {
                        //converting it as NSDictionary
                        //let jsonData = result as? NSDictionary
                        //self.activityIndicator.stopAnimating()
                        
                        if ((result as? NSDictionary)?.value(forKey: "message") != nil)
                        {
                            // Cast json post data as NSDict
                            let jsonData = result as? NSDictionary
                            
                            let errorMessage = jsonData?.value(forKey: "message") as? String
                            
                            if let error = errorMessage {
                                print(error)
                            }
                        }
                        else if ((result as? NSDictionary) != nil)
                        {
                            
                            let jsonData = result as? NSDictionary
                            
                            // Cast each post dict objct as a Post objct
                            if let page = jsonData {
                                
                                let pageObjct = Post(post: page)
                                
                                if let content = pageObjct.content {
                                    self.webView.loadHTMLString("\(MyGlobalClass.htmlStyleBegin1)\(content.replacingOccurrences(of: "dir=\"ltr\"", with: "").replacingOccurrences(of: "class=\"table table-striped\"", with: "class=\"table\""))\(MyGlobalClass.htmlStyleEnd)", baseURL: nil)
                                    print("\(MyGlobalClass.htmlStyleBegin1)\(content.replacingOccurrences(of: "dir=\"ltr\"", with: "").replacingOccurrences(of: "class=\"table table-striped\"", with: "class=\"table\""))\(MyGlobalClass.htmlStyleEnd)")
                                }
                            }
                        }
                    }
                case .failure( _):
                    self.funcToCallCalledWhenUIWebViewFinishesLoading()
                    NetworkActivityIndicatorManager.networkOperationFinished()
                    if let error = response.result.error as? URLError {
                        MyGlobalClass.showAlert(title: "Oops!", message: error.localizedDescription, view: self)
                    } else {
                        MyGlobalClass.showAlert(title: "Oops!", message: "Something went wrong, try again later.", view: self)
                    }
                    return
                }
        }
    }
}

// MARK: UIWebViewDelegate

extension WebViewController: UIWebViewDelegate {
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        funcToCallCalledWhenUIWebViewFinishesLoading()
        NetworkActivityIndicatorManager.networkOperationFinished()
        webView.resizeWebContent()
    }
   
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if navigationType == .linkClicked {
            if let storyboard = self.storyboard {
                let VC = storyboard.instantiateViewController(withIdentifier: "StandardWebViewController") as! StandardWebViewController
                VC.url = request.url?.absoluteString
                self.navigationController?.pushViewController(VC, animated: true)
            }
            return false
        }
        
        return true  //Changed line.
    }
}

// MARK: UIScrollViewDelegate

extension WebViewController: UIScrollViewDelegate {
    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if (scrollView.contentOffset.x > 0){
//            scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentOffset.y)
//        }
//    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(velocity.y>0) {
            //Code will work without the animation block.I am using animation block incase if you want to set any delay to it.
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
                //self.navigationController?.setToolbarHidden(true, animated: true)
                print("Hide")
            }, completion: nil)
            
        } else {
            UIView.animate(withDuration: 2.5, delay: 0, options: UIViewAnimationOptions(), animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
                //self.navigationController?.setToolbarHidden(false, animated: true)
                print("Unhide")
            }, completion: nil)
        }
    }
}

