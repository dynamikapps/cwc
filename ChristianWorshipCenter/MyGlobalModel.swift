//
//  MyGlobalModel.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/29/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import Foundation

class MyGlobalClass {
    
    // Global variable referred to AppDelegate to call from any class
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    // Global info view colors
    // let redColor = ColorComponents.RGB(red: 255, green: 50, blue: 75, alpha: 1).color()
    
    static let brandPurple = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
    
    // Globel info view label sizse
    static let fontSize12 = UIScreen.main.bounds.width / 31
    
    static let htmlStyleBegin = "<html><head><meta charset=\"UTF-8\"><style> body { color: #555555; font-family:Helvetica; font-size: 14px } h1 { color: #ffffff; font-family:Chalkduster; font-size: 18pt } a { font-family:Helvetica; font-weight: bold; color: #5ABBB5; text-decoration:none; padding-left: 5px; } img { padding-left:0; align: center; max-width: 100%; max-height: 100%} iframe { padding-left:0; align: center; max-width: 100%; max-height: 100%} .content { padding-left: 1em; padding-top: 1em; }</style></head><body>"
    
    static let htmlStyleBegin1 = "<html><head><meta charset=\"utf-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"><link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"><script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script><script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script><style> body {padding: 10px 10px 10px 10px;} img {padding: 10px 10px 10px 10px; align: center; max-width: 100%; max-height: 100%} iframe {padding: 10px 10px 10px 10px; align: center; max-width: 100%; max-height: 100%} .content {padding: 10px 10px 10px 10px;} td {float:none; display:block;} table {width: 100%; border: 5;} </style></head><body>"
    
    static let htmlStyleEnd = "</body></html>"
    
    // MARK: - View Alert controller
    static func showAlert(title: String, message: String, style: UIAlertControllerStyle = .alert, view: AnyObject){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        view.present(alertController, animated: true, completion: nil)
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(cancelAction)
    }
}

// Create status bar frame color
extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

// MARK: - Remove protocol identifier from URL

let myPIds = ["www.", "http://www.", "http://", "Www.", "Http://www.", "Http://",
              "WWW.", "HTTP://WWW.", "HTTP://", "https://www.", "https://", "Https://www.", "Https://", "HTTPS://www.", "HTTPS://"]

extension String {
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.characters.filter({ !characters.contains($0) }))
    }
    
    func stringByRemovingAll(subStrings: [String]) -> String {
        var resultString = self
        subStrings.map { resultString = resultString.replacingOccurrences(of: $0, with: "")
        }
        return resultString
    }
}
