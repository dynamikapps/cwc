//
//  MenuTableViewController.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/24/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    // Class Objects
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var logoBgView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let bgView = UITapGestureRecognizer(target: self, action: #selector(MenuTableViewController.showHomePage))
        logoBgView.addGestureRecognizer(bgView)
        logoBgView.isUserInteractionEnabled = true
    }
    
    // Show home page function
    func showHomePage() {
        if let storyboard = storyboard {
            
            let homeViewController = storyboard.instantiateViewController(withIdentifier: "HomeCollectionViewController") as! HomeCollectionViewController
            
            let homeViewNav = UINavigationController(rootViewController: homeViewController)
            
            homeViewNav.isNavigationBarHidden = false
            homeViewNav.navigationBar.isTranslucent = false
            homeViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
            homeViewNav.navigationBar.shadowImage = UIImage()
            homeViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
            homeViewNav.navigationBar.tintColor = UIColor.white
            homeViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            
            let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            
            appDelegate.drawerContainer!.centerViewController = homeViewNav
            appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    // Assign header background color and text color
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        let header = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
        header.textLabel?.textColor = UIColor.white
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        var returnValue = 0
        
        // Section number is interger start at 0
        switch section {
        // Shows and controls Favs Segment
        case 0:
            returnValue = 4
            break
        // Shows and controls Everyone Segment
        case 1:
            returnValue = 2
            break
        // Shows and controls Everyone Segment
        case 2:
            returnValue = 5
            break
        default:
            break
        }
        
        return returnValue
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    // Check which section and rows number is selected
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)
        
        switch (indexPath.section, indexPath.row) {
            
        //
        case (0, 0):
            //
            print("CWC tapped.")
            if let storyboard = storyboard {
                
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/14"
                webViewController.pageTitle = "CWC"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (0, 1):
            //
            print("History tapped.")
            if let storyboard = storyboard {
                
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/104"
                webViewController.pageTitle = "History"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (0, 2):
            //
            print("Pastors tapped.")
            if let storyboard = storyboard {
                
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/21"
                webViewController.pageTitle = "Pastors"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (0, 3):
            //
            print("Staff tapped.")
            if let storyboard = storyboard {
                
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/56"
                webViewController.pageTitle = "Staff"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (1, 0):
            //
            print("Leesburg tapped.")
            if let storyboard = storyboard {
                
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/94"
                webViewController.pageTitle = "Leesburg"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (1, 1):
            //
            print("South Lake tapped.")
            if let storyboard = storyboard {
                let webViewController = storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                webViewController.url = "http://christianworshipcenters.com/wp-json/wp/v2/pages/96"
                
                webViewController.pageTitle = "South Lake"
                
                let webViewNav = UINavigationController(rootViewController: webViewController)
                
                webViewNav.isNavigationBarHidden = false
                webViewNav.navigationBar.isTranslucent = false
                webViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                webViewNav.navigationBar.shadowImage = UIImage()
                webViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                webViewNav.navigationBar.tintColor = UIColor.white
                webViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.drawerContainer!.centerViewController = webViewNav
                appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (2, 0):
            //
            print("PayPal tapped.")
            if let storyboard = storyboard {
                
                let popWebViewController = storyboard.instantiateViewController(withIdentifier: "PopWebViewController") as! PopWebViewController
                popWebViewController.url = "https://itunes.apple.com/us/app/paypal-mobile-cash/id283646709?mt=8"
                //popWebViewController.pageTitle = "PayPal"
                
                let popWebViewNav = UINavigationController(rootViewController: popWebViewController)
                
                popWebViewNav.isNavigationBarHidden = false
                popWebViewNav.navigationBar.isTranslucent = false
                popWebViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                popWebViewNav.navigationBar.shadowImage = UIImage()
                popWebViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                popWebViewNav.navigationBar.tintColor = UIColor.white
                popWebViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(popWebViewNav, animated: true, completion: nil)
                
                //let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                //appDelegate.drawerContainer!.centerViewController = webViewNav
                //appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (2, 1):
            //
            print("Givelify tapped.")
            if let storyboard = storyboard {
                
                let popWebViewController = storyboard.instantiateViewController(withIdentifier: "PopWebViewController") as! PopWebViewController
                popWebViewController.url = "https://itunes.apple.com/us/app/givelify/id725815127"
                //popWebViewController.pageTitle = "Givelify"
                
                let popWebViewNav = UINavigationController(rootViewController: popWebViewController)
                
                popWebViewNav.isNavigationBarHidden = false
                popWebViewNav.navigationBar.isTranslucent = false
                popWebViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                popWebViewNav.navigationBar.shadowImage = UIImage()
                popWebViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                popWebViewNav.navigationBar.tintColor = UIColor.white
                popWebViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(popWebViewNav, animated: true, completion: nil)
                
                //let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                //appDelegate.drawerContainer!.centerViewController = webViewNav
                //appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (2, 2):
            //
            print("Events tapped.")
            if let storyboard = storyboard {
                
                let popWebViewController = storyboard.instantiateViewController(withIdentifier: "PopWebViewController") as! PopWebViewController
                popWebViewController.url = "http://christianworshipcenters.com/calendar/"
                //popWebViewController.pageTitle = "Events"
                
                let popWebViewNav = UINavigationController(rootViewController: popWebViewController)
                
                popWebViewNav.isNavigationBarHidden = false
                popWebViewNav.navigationBar.isTranslucent = false
                popWebViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                popWebViewNav.navigationBar.shadowImage = UIImage()
                popWebViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                popWebViewNav.navigationBar.tintColor = UIColor.white
                popWebViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(popWebViewNav, animated: true, completion: nil)
                
                //let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                //appDelegate.drawerContainer!.centerViewController = webViewNav
                //appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (2, 3):
            //
            print("Contact Us tapped.")
            if let storyboard = storyboard {
                
                let popWebViewController = storyboard.instantiateViewController(withIdentifier: "PopWebViewController") as! PopWebViewController
                popWebViewController.url = "http://christianworshipcenters.com/contact-us/"
                //popWebViewController.pageTitle = "Contact Us"
                
                let popWebViewNav = UINavigationController(rootViewController: popWebViewController)
                
                popWebViewNav.isNavigationBarHidden = false
                popWebViewNav.navigationBar.isTranslucent = false
                popWebViewNav.navigationBar.barTintColor = UIColor(red: 89/255, green: 65/255, blue: 149/255, alpha: 1)
                popWebViewNav.navigationBar.shadowImage = UIImage()
                popWebViewNav.navigationBar.setBackgroundImage(UIImage(), for: .default)
                popWebViewNav.navigationBar.tintColor = UIColor.white
                popWebViewNav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.navigationController?.present(popWebViewNav, animated: true, completion: nil)
                
                //let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                //appDelegate.drawerContainer!.centerViewController = webViewNav
                //appDelegate.drawerContainer!.toggle(MMDrawerSide.left, animated: true, completion: nil)
            }
            break
        case (2, 4):
            //
            print("Share this app.")
            
            let height = UIScreen.main.bounds.height
            
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                // It's an iPhone
                // Share tapped
                let firstActionItem = "Download the CWC app now from the App Store. https://itunes.apple.com/us/app/christian-worship-center/id1271237368?ls=1&mt=8"
                
                let actionViewController = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
                self.present(actionViewController, animated: true, completion: nil)
            case .pad:
                // It's an iPad
                let firstActionItem = "Download the CWC app now from the App Store. https://itunes.apple.com/us/app/christian-worship-center/id1271237368?ls=1&mt=8"
                
                let activityVC = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
                let nav = UINavigationController(rootViewController: activityVC)
                nav.modalPresentationStyle = UIModalPresentationStyle.popover
                let popover = nav.popoverPresentationController as UIPopoverPresentationController!
                activityVC.preferredContentSize = CGSize(width: height / 4.55, height: height / 4.55)
                popover?.sourceView = cell?.textLabel
                popover?.sourceRect = CGRect(x: height / 7.0, y: height / 37.0, width: 0.0, height: 0.0)
                
                self.present(nav, animated: true, completion: nil)
                
            case .unspecified:
                // Share tapped
                let firstActionItem = "Download the CWC app now from the App Store. https://itunes.apple.com/us/app/christian-worship-center/id1271237368?ls=1&mt=8"
                
                let actionViewController = UIActivityViewController(activityItems: [firstActionItem], applicationActivities: nil)
                self.present(actionViewController, animated: true, completion: nil)
            default:
                break
            }
        default:
            break
        }
    }
}
