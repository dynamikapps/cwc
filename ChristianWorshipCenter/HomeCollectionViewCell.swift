//
//  HomeCollectionViewCell.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/29/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         myImageView.layer.cornerRadius =  10.0
         myImageView.clipsToBounds = true
    }
}
