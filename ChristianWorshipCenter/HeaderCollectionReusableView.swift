//
//  HeaderCollectionReusableView.swift
//  ChristianWorshipCenter
//
//  Created by Andy Metellus on 3/29/17.
//  Copyright © 2017 NJ VAUGHN MEDIA, LLC. All rights reserved.
//

import UIKit
import LIHImageSlider

class HeaderCollectionReusableView: UICollectionReusableView{
    
    // Header view text label
    @IBOutlet weak var textLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
